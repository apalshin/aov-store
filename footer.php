<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

        </div><!-- .site-content -->


        <footer id="colophon" class="site-footer" role="contentinfo">

	        <div class="footer-main">
		        <div class="container">
			        <div class="footer-logo">
				        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/footer_logo.png" class="img-fluid"/>
			        </div>
			        <div class="row">
				        <div class="col-12 col-md-6">
					        <div class="row">
						        <div class="col-12 col-md-6">
							        <div class="subscribe-form">
								        <div class="top-20"></div>
								        <h4>Подписаться на новости</h4>
								        <div class="top-40"></div>
								        <div class="emaillist">
									        <form action="#" method="post" class="es_subscription_form es_shortcode_form" id="es_subscription_form_1569418315" data-source="ig-es" _lpchecked="1">
										        <div class="es-field-wrap">
											        <input class="es_required_field es_txt_email form-control form-control" type="email" name="email" value="" placeholder="E-mail" required=""></label>
										        </div>
										        <input type="hidden" name="lists[]" value="2">
										        <input type="hidden" name="form_id" value="1">
										        <input type="hidden" name="es_email_page" value="27">
										        <input type="hidden" name="es_email_page_url" value="<?php echo site_url(); ?>">
										        <input type="hidden" name="status" value="Unconfirmed">
										        <input type="hidden" name="es-subscribe" id="es-subscribe" value="36c1da971c">
										        <label style="position:absolute;top:-99999px;left:-99999px;z-index:-99;"><input type="text" name="es_hp_985455a8fa" class="es_required_field" tabindex="-1" autocomplete="-1"></label>
										        <input type="submit" name="submit" class="es_subscription_form_submit es_submit_button es_textbox_button" id="es_subscription_form_submit_1569418315" value="Subscribe" style="opacity: 0">
										        <span class="es_spinner_image" id="spinner-image"><img src="http://aovloc.com/wp-content/plugins/email-subscribers/public/images/spinner.gif"></span>

									        </form>

									        <span class="es_subscription_message" id="es_subscription_message_1569418315" style="color: #ffffff;"></span>
								        </div>
							        </div>
							        <div class="top-40"></div>
							        <figure class="footer-social-menu">
								        <ul>

									        <?php
									        // check if there is Social Media group
									        if( have_rows('social_media', 27) ):
										        while ( have_rows('social_media', 27) ) : the_row();
											        $social = get_sub_field('social_media_fields');
											        ?>

											        <li>
												        <a href="<?php echo $social["social_media_link"]; ?>" class="d-inline-block"><?php echo $social["social_media_name"]; ?></a>
											        </li>

										        <?php
										        endwhile;
									        endif;
									        ?>
								        </ul>
							        </figure>
						        </div>
					        </div>
				        </div>
				        <div class="col-12 col-md-6">
					        <div class="top-20">
						        <div class="row">
							        <div class="col-6">
								        <?php
								        if( has_nav_menu('footer_1') ) {
									        wp_nav_menu(array(
										        'theme_location' => 'footer_1',
										        'container' => false
									        ));
								        }
								        ?>
							        </div>
							        <div class="col-6">
								        <?php
								        if(has_nav_menu('footer_2')) {
									        wp_nav_menu(array(
										        'theme_location' => 'footer_2',
										        'container' => false,
									        ));
								        }
								        ?>
							        </div>
						        </div>
					        </div>
				        </div>

			        </div>

			        <div class="top-30 copyright"><?php _e( '&copy; 2019 Ovchinnikov Jewelry. Все права защищены', 'aovchinnikov' ); ?></div>

		        </div>
	        </div>

        </footer><!-- .site-footer -->

    </div><!-- .site -->

    <?php wp_footer(); ?>

	<figure class="popup-window registration-modal" style="display: none;">
		<div class="modal-body">
			<div class="close-modal">Close</div>
			<h2><?php _e( 'Registration Form','aveltheme' ); ?></h2>
			<div class="top-1r"></div>
			<div class="row">
				<div class="col-lg-8">
					<div><?php _e( 'Insert your personal data to create the account.','aveltheme' ); ?></div>
					<div class="top-3r"></div>
					<div class="row no-gutters">
						<div class="col-md-4">
							<p>
								<input type="text" name="email" class="form-control" placeholder="Email *" />
							</p>
						</div>
						<div class="col-md-4">
							<p>
								<input type="password" name="password" class="form-control" placeholder="Password" />
							</p>
						</div>
					</div>
					<div class="top-05r hidden-mobile"></div>
					<div class="row no-gutters">
						<div class="col-md-4">
							<p>
								<input type="text" name="username" class="form-control" placeholder="Username *" />
							</p>
						</div>
						<div class="col-md-4">
							<p>
								<input type="text" name="name" class="form-control" placeholder="Name" />
							</p>
						</div>
						<div class="col-md-4">
							<p>
								<input type="text" name="surname" class="form-control" placeholder="Surname" />
							</p>
						</div>
					</div>
					<div class="top-05r hidden-mobile"></div>
					<div class="row no-gutters">
						<div class="col-md-4">
							<p>
								<input type="text" name="country" class="form-control" placeholder="Country" />
							</p>
						</div>
					</div>
					<div class="top-3r">
						<p><i>* This field is mandatory.</i></p>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="row">
						<div class="col-lg-1"></div>
						<div class="col-lg-11">
							<div>
								<p><?php _e( 'I read and understand the privacy policy and I am more than 16 years old.','aveltheme' ); ?></p>
							</div>
							<div class="top-2r">
							</div>
							<div class="top-2r">
								<button class="btn btn-submit"><?php _e( 'Submit','aveltheme' ); ?></button>
							</div>
						</div>
					</div>

				</div>
			</div>


		</div>

	</figure>

</body>
</html>
