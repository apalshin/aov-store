<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:500,600,700&display=swap&subset=cyrillic" rel="stylesheet">    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <!--[if lt IE 9]>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/html5.js"></script>
    <![endif]-->
	<?php wp_head(); ?>


</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
    <a class="skip-link screen-reader-text hidden" href="#content"><?php _e( 'Skip to content', 'twentyfifteen' ); ?></a>

	<?php

	if ( get_field('inverted_header')[0] == 'Yes' ) { $inv_header = "inverted"; } else { $inv_header = ""; }

	?>

    <header id="masthead" class="site-header <?php echo $inv_header; ?>" role="banner">

	    <nav class="navbar" role="navigation">

		    <a class="navbar-brand hidden-mobile d-sm-none d-xl-block" href="<?php echo site_url(); ?>">
			    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/img/mainlogo.png"/>
		    </a>

		    <div class="container-fluid pl-0 pr-0">
			    <div class="row" style="flex-grow: 1">

				    <div class="col-4 d-flex justify-content-start">

					    <button class="navbar-close" aria-label="Toggle navigation" aria-expanded="true">
						    <span class="navbar-toggler-icon"></span>
					    </button>

					    <button class="navbar-toggler head-menu-btn" type="button" aria-expanded="false" aria-label="Toggle navigation" data-toggle="collapse" href="#menuCollapse" aria-controls="menuCollapse">
						    <span class="navbar-toggler-icon"></span>
					    </button>

					    <a class="nav-search-toggle head-menu-btn" data-toggle="collapse" href="#searchCollapse" role="button" aria-expanded="false" aria-controls="searchCollapse"></a>

				    </div>

				    <div class="col-4">

					    <a class="navbar-brand d-xl-none" href="<?php echo site_url(); ?>">
						    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/img/mobilelogo.png"/>
					    </a>

				    </div>

				    <div class="col-4 d-flex justify-content-end">

					    <!--<a class="cart-link pull-right head-menu-btn" href="/cart"></a>-->

					    <a class="cart-contents cart-link pull-right head-menu-btn" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><span class="items-<?php echo WC()->cart->get_cart_contents_count(); ?>"><?php echo WC()->cart->get_cart_contents_count(); ?></span></a>

					    <?php if (is_user_logged_in()) { ?>
						    <div class="dropdown">
							    <a class="myaccount-link pull-right head-menu-btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>

							    <div class="header-dropdown-menu myaccount-dropdown dropdown-menu" aria-labelledby="dropdownMenuLink">

								    <?php
								    foreach ( wc_get_account_menu_items() as $endpoint => $label ) { ?>
								    	<a class="dropdown-item" href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>"><?php echo esc_html( $label ); ?></a>
								    <?php } ?>

							    </div>
						    </div>

					    <?php } else { ?>
					        <a class="account-link myaccount-link pull-right head-menu-btn" data-toggle="collapse" href="#loginCollapse" role="button" aria-expanded="false" aria-controls="loginCollapse""></a>
						<?php } ?>
				    </div>

			    </div>

		    </div>

		    <figure class="collapsable-wrapper search-collapse collapse head-collapse" id="searchCollapse">

			    <div class="top-40"></div>

			    <div class="container">
				    <div class="row">
					    <div class="col-xl-1"></div>
					    <div class="col-md-6 col-xl-5">
						    <h2><?php _e( 'Поиск', 'aovchinnikov' ); ?></h2>
					    </div>
					    <div class="col-md-6 col-xl-5">
						    <form role="search" method="get" id="searchform" class="nav-searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							    <div>
								    <label class="screen-reader-text" for="s"><?php _x( 'Search for:', 'label' ); ?></label>
								    <input class="form-control" type="text" name="s" value="<?php echo get_search_query(); ?>" id="s" placeholder="<?php _e( 'Серьги Лёд', 'aovchinnikov' ); ?>" autocomplete="on"woocommerce-form-register/>
								    <input class="btn submit-btn" type="submit" id="searchsubmit" value="" />
							    </div>
						    </form>
					    </div>
				    </div>

				    <div class="row">
					    <div class="col-xl-1"></div>
					    <div class="col-md-6 col-xl-5">
						    <div id="header_search_result" class="results-wrapper">
						    </div>
					    </div>
					    <div class="col-md-6 col-xl-5">
						    <div id="header_search_item" class="search-item-wrapper">
						    </div>
					    </div>
				    </div>

			    </div>

		    </figure>

		    <figure class="collapsable-wrapper login-collapse collapse head-collapse" id="loginCollapse">

			    <section class="stage-1 login-wrapper">

				    <div class="container">

					    <div id="loginAccordion">

						    <div id="collapseOne" class="collapse show" data-parent="#loginAccordion">

							    <div class="row">
								    <div class="col-xl-1"></div>
								    <div class="col-md-6 col-xl-5">
									    <h2><?php _e( 'Авторизация', 'aovchinnikov' ); ?></h2>
								    </div>
								    <div class="col-md-6 col-xl-5">

									    <div class="row">
										    <div class="col">
											    <input type="text" class="form-control" name="username" id="userNameStepOne" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" placeholder="<?php _e( 'Имя пользователя или Email *', 'aovchinnikov' ); ?>"/>
										    </div>
									    </div>

									    <div class="top-40"></div>

									    <div class="row">
										    <div class="col">
											    <button type="button" class="btn btn-lg btn-block btn-blue" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" id="headLoginNext" disabled><?php _e( 'Далее', 'aovchinnikov' ); ?></button>
										    </div>
										    <div class="col">
											    <a href="<?php home_url();?>/my-account/" class="btn btn-lg btn-block btn-transp"><?php _e( 'Регистрация', 'aovchinnikov' ); ?></a>
										    </div>
									    </div>

									    <div class="top-50">
										    <a class="btn btn-link btn-lg btn-fb btn-arrow" href="http://aovloc.com/wp-login.php?loginSocial=facebook" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="facebook" data-popupwidth="475" data-popupheight="175">
											    <i class="fa fa-facebook"></i>&nbsp;&nbsp;&nbsp;<?php _e( 'Войти с помощью Facebook', 'aovchinnikov' ); ?>
										    </a>
									    </div>

									    <div class="top-50"></div>

								    </div>
							    </div>

						    </div>

						    <div id="collapseTwo" class="collapse" data-parent="#loginAccordion">

							    <div class="row">
								    <div class="col-xl-1"></div>
								    <div class="col-md-6 col-xl-5">
									    <h2><?php _e( 'Авторизация', 'aovchinnikov' ); ?></h2>
								    </div>
								    <div class="col-md-6 col-xl-5">

									    <?php do_action( 'woocommerce_before_customer_login_form' ); ?>

									    <form id="loginForm" class="woocommerce-form woocommerce-form-login login" method="post">

											    <?php do_action( 'woocommerce_login_form_start' ); ?>

										    <div class="row">
											    <div class="col-10">
												    <span id="loginUserName" class="login-email">&nbsp;</span>
											    </div>
											    <div class="col-2">
												    <button onclick="$('#loginStatus').hide(); return true;" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne" style="padding: 0"><i class="fa fa-pencil"></i></button>
											    </div>
										    </div>

										    <div class="top-40"></div>

										    <div class="row">
											    <div class="col">
												    <input type="password" class="form-control" name="password" id="headPassword" autocomplete="current-password" placeholder="<?php _e( 'Пароль', 'aovchinnikov' ); ?>"/>
												    <img width="22" id="showPass" class="show-pass-toggle" src="<?php echo get_stylesheet_directory_uri(); ?>/img/show_pass.png"/>
											    </div>
										    </div>

										    <div class="top-20"></div>

										    <div class="row">
											    <div class="col">
												    <label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
													    <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="headRememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
												    </label>
											    </div>
										    </div>

										    <div class="top-20"></div>

										    <?php do_action( 'woocommerce_login_form' ); ?>

										    <div id="loginStatus" class="alert alert-danger" style="display: none"></div>

										    <div class="row">
											    <div class="col">
												    <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
												    <button id="loginSubmit" type="button" class="btn btn-lg btn-block btn-blue woocommerce-button button woocommerce-form-login__submit" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php _e( 'Войти', 'aovchinnikov' ); ?></button>

											    </div>
											    <div class="col">
												    <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="btn btn-lg btn-block btn-transp"><?php _e( 'Восстановить пароль', 'aovchinnikov' ); ?></a>
											    </div>
										    </div>

										    <div class="top-50"></div>

									    </form>

								    </div>
							    </div>

							    <?php do_action( 'woocommerce_login_form_end' ); ?>

						    </div>

				    </div>

			    </section>

		    </figure>

		    <figure class="collapsable-wrapper menu-collapse collapse head-collapse" id="menuCollapse">

			    <div class="top-40"></div>

			    <div class="container">

				    <div class="row">

					    <div class="col">

						    <?php
						    // Primary navigation menu.
						    wp_nav_menu(array(
							    'theme_location' => 'primary',
							    'depth'	          => 2, // 1 = no dropdowns, 2 = with dropdowns.
							    'container'       => 'div',
							    'container_class' => 'navbar-collapse',
							    'container_id'    => 'bs-navbar',
							    'menu_class'      => 'navbar-nav mr-auto',
							    'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
							    'walker'          => new WP_Bootstrap_Navwalker(),
						    ));

						    ?>

					    </div>

				    </div>



			    </div>

		    </figure>

	    </nav>

    </header><!-- .site-header -->

    <div class="site-content">