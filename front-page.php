<?php
/* Template Name: Front page */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
            <figure class="front-page">

	            <section class="front-page-slider">

		            <div class="front-slider-wrap">
			            <?php
			            // check if there is Home Banner group
			            if( have_rows('home_banner') ):
				            while ( have_rows('home_banner') ) : the_row();
					            ?>

					            <figure class="slide" data-slide="<?php the_sub_field( 'slide' ); ?>" style="background-image: url('<?php the_sub_field( 'slide' ); ?>')">
						            <div class="next-slide"></div>
					            </figure>

				            <?php
				            endwhile;
			            endif;
			            ?>
		            </div>

		            <figure class="banner-overlay">
			            <?php
			            // check if there is Banner Overlay group
			            if( have_rows('banner_overlay') ):
				            while ( have_rows('banner_overlay') ) : the_row();
					            ?>

					            <h1><?php the_sub_field( 'title' ); ?></h1>
					            <div class="top-10">
						            <p><?php the_sub_field( 'sub_title' ); ?></p>
					            </div>
					            <div class="top-50">
						            <a href="<?php the_sub_field( 'button_link' ); ?>" class="btn btn-transp btn-arrow"><?php the_sub_field( 'button_text' ); ?></a>
					            </div>

				            <?php
				            endwhile;
			            endif;
			            ?>
		            </figure>

		            <figure class="banner-social-menu">
			            <ul>
				            <?php
				            // check if there is Social Media group
				            if( have_rows('social_media') ):
					            while ( have_rows('social_media') ) : the_row();
						            $social = get_sub_field('social_media_fields');
						            ?>

						            <li>
							            <a href="<?php echo $social["social_media_link"]; ?>" class="d-inline-block"><?php echo $social["social_media_name"]; ?></a>
						            </li>

				                <?php
				                endwhile;
				            endif;
				            ?>

			            </ul>
		            </figure>

	            </section>


	            <section class="front-page-cats-slider">

		            <div class="container-fluid">

			            <div class="front-cats-wrap">

				            <?php
				            // check if there is Expertise items group
				            if( have_rows('categories') ):
					            while ( have_rows('categories') ) : the_row();
						            if( have_rows('category_fields') ):
							            while ( have_rows('category_fields') ) : the_row();
								            if( get_row_layout() == 'category_layout' ):?>
									            <a href="<?php the_sub_field('category_link'); ?>">
										            <div class="category-box hoverbox">
											            <div class="hover-box-inner">
												            <div class="category-image" style="background-image: url('<?php the_sub_field('category_image'); ?>')"></div>
												            <div class="hover-fadein top-50 text-center">
													            <h2><?php the_sub_field('category_title'); ?></h2>
													            <div class="top-10">
														            <p><?php the_sub_field('category_subtitle'); ?></p>
													            </div>
													            <div class="top-30"></div>
												            </div>
											            </div>
										            </div>
									            </a>
								            <?php
								            endif;
							            endwhile;
						            endif;
					            endwhile;
				            endif;
				            ?>

			            </div>

		            </div>

	            </section>

	            <div class="top-50"></div>
	            <div class="top-30"></div>

	            <section class="front-featured">

		            <div class="container">

			            <h2>Рекомендации</h2>

			            <div class="top-50"></div>

			            <?php
			            // check if there is Home Banner group
			            if( $product_ids = get_field( 'featured_items' ) ): ?>

				            <?php echo do_shortcode('[products ids="'.$product_ids.'"]'); ?>

			            <?php endif; ?>

		            </div>

	            </section>

	            <div class="top-50"></div>
	            <div class="top-30"></div>

	            <section class="front-news">

		            <div class="container">

			            <h2>Новости</h2>

			            <div class="top-50"></div>

			            <div class="row">

				            <?php

							    $args = array(
								    'posts_per_page'=> 2,
								    'post_status' => 'publish',
								    'post_type' => 'news',
							    );
							    // Start the Loop.
							    $myposts = get_posts( $args );
							    foreach ( $myposts as $post ) : setup_postdata( $post ); ?>

								    <div class="col-12 col-md-6 pl-0 pr-0">

									    <a href="<?php echo get_permalink(); ?>">

										    <figure class="front-news-item hoverbox">

											    <div class="hover-box-inner news-item-image" style="background-image: url('<?php echo get_the_post_thumbnail_url();?>')"></div>

											    <div class="top-20">

												    <div class="news-item-name">
													    <div class="news-date"><?php the_time('d.m.Y'); ?></div>
													    <h4 class="top-10"><?php the_title(); ?></h4>
													    <div class="top-30 read-more hover-fadein"><span>Читать новость</span></div>
												    </div>

											    </div>

										    </figure>

									    </a>

								    </div>

			                    <?php
								// End the loop.
			                    endforeach;
							    wp_reset_postdata();
				            ?>

			            </div>

		            </div>

	            </section>

	            <div class="top-30"></div>
	            <div class="top-30"></div>

	            <?php

	            /*

	            // check if there is Home Banner group
	            if( have_rows('home_banner') ):

		            while ( have_rows('home_banner') ) : the_row();

			            if( get_row_layout() == 'banner_fields' ):

				            ?>

				            <section class="main-banner" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/home/banner_bg.jpg');">

					            <img class="banner-right" src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/banner_right.png"/>

					            <div class="container-fluid">

						            <div class="row">
							            <div class="col-sm-5 text-center">
								            <img class="img-fluid banner-left" src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/banner_left.png"/>
							            </div>
							            <div class="col-sm-2"></div>
							            <div class="col-sm-5 text-center">
								            <img class="img-fluid banner-logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/home/banner_logo.png"/>
							            </div>
						            </div>

					            </div>

				            </section>

				            <?php

			            endif;

		            endwhile;

	            else :

		            // no layouts found
		            echo "No data found.";

	            endif;

	            */

	            ?>

            </figure>
		</main><!-- .site-main -->
	</div><!-- .content-area -->


<?php get_footer(); ?>
