<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php
			// Start the loop.
			while ( have_posts() ) :
				the_post();

			?>
			<div class="row">
				<div class="col-xl-10 offset-xl-1">

					<?php

					// Include the single post content template.
					get_template_part( 'template-parts/content', 'single' );

					?>

				</div>
			</div>

			<?php

				// End of the loop.
			endwhile;
			?>

		</main><!-- .site-main -->

		<?php get_sidebar( 'content-bottom' ); ?>

	</div><!-- .content-area -->
</div>

<?php get_footer(); ?>
