	+function ($) {

		/* Fancybox init */
		$('.fancybox-link').fancybox({
			openEffect  : 'none',
			closeEffect : 'none',
			helpers : {
				media : {}
			}
		});

		/* Scroll to ID button */

	    function goToByScroll(id){
	        // Scroll
	        $('html,body').animate({
	                scrollTop: $("#"+id).offset().top-50},
	            'slow');
	    }


	    /* Email validation with regexp */

		function isValidEmailAddress(emailAddress) {
			var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
			return pattern.test(emailAddress);
		}


	    /* Modal windows behavior */

		function close_modal(modal) {
			$(".alerts").html('');
			$("html").removeClass("noscroll");
			$(modal).closest("figure.popup-window").removeClass('visible').hide();
		}

		$( ".modal-button" ).click(function (event) {
			var target = "figure.popup-window."+$(this).attr("data-modal");
			$("html").addClass("noscroll");
			$(target).show().addClass("visible");
			event.preventDefault();
		});

		function add_form_error_message(element,message) {
			$(element).parent().append(message).addClass("form-error");
		}

		/* Add .form-control to variations selects */
		$('form.variations_form').each( function () {
			$(this).find('select').addClass('form-control');
			$(this).find('input.qty').addClass('form-control');
		});

		$('form.woocommerce-checkout').each( function () {
			$(this).find('.form-row').addClass('form-group').removeClass('form-row');
			$(this).find('input[type=text]').addClass('form-control');
			$(this).find('input[type=tel]').addClass('form-control');
			$(this).find('input[type=email]').addClass('form-control');
			$(this).find('textarea').addClass('form-control');
		});


		/* Add class to <body> when collapsable menu is visible */

		$( "button.navbar-toggler" ).on( "click", function () {
			$( "body" ).toggleClass( "menu-visible" );
		});

		$( "button.navbar-close" ).on( "click", function () {
			$( "body" ).removeClass( "menu-visible" );
			$( ".head-collapse" ).collapse( "hide" );
		});


		/* Search panel toggle */

		$( "a.nav-search-toggle" ).on("click", function (e) {
			$( "body" ).toggleClass( "menu-visible" );
			$( "input#s" ).focus();
		});


		$( "a.account-link" ).on("click", function (e) {
			$( "body" ).toggleClass( "menu-visible" );
		});


		$( "#headLoginNext" ).click( function (e) {
			var username = $("#userNameStepOne").val();

			$( "#loginForm" ).attr("data-username", username );
			$( "#loginUserName" ).text( username );
		});


		$( "#userNameStepOne" ).on( "change keypress", function () {
			if ( ( $(this).val() ) !=='' ) {
				$( "#headLoginNext" ).removeAttr( "disabled" );
			} else {
				$( "#headLoginNext" ).attr( "disabled", "disabled" );
			}
		}).keypress();


		$( "#showPass" ).click( function () {
			var x = document.getElementById("headPassword");

			console.log(x);
			if (x.type === "password") {
				x.type = "text";
			} else {
				x.type = "password";
			}
		});

		/* AJAX login */

		// Perform AJAX login on form submit
		$('#loginSubmit').on('click', function(e){
			var button = $(this);
			var buttonHtml = $(this).html();
			var username = $('form#loginForm').data('username');
			var userpassword = $('form#loginForm input#headPassword').val();

			console.log(userpassword);

			button.html('<i class="fa fa-circle-o-notch fa-spin"></i>').attr('disabled','disabled');

			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: AOV.ajaxurl,
				data: {
					'action': 'aov_ajax_login',
					'username': username,
					'password': userpassword,
					'security': AOV.ajax_nonce
				},
				success: function(data){
					if (data.loggedin == true){
						document.location.href = AOV.redirecturl;
					} else {
						$('#loginStatus').text(data.message).show();
						button.html(buttonHtml);
						button.removeAttr('disabled');
					}
				}
			});
			e.preventDefault();
		});


		/* AJAX registration - not used for ver 1.0 */

		// Perform AJAX login on form submit
		/*$('#regSubmit').on('click', function(e){
			var button = $(this);
			var buttonHtml = $(this).html();
			var reg_email = $('form#regForm input#reg_email').val();
			//var reg_username = $('form#regForm input#reg_username').val();
			//var reg_password = $('form#regForm input#reg_password').val();

			button.html('<i class="fa fa-circle-o-notch fa-spin"></i>').attr('disabled','disabled');

			console.log(reg_email);

			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: AOV.ajaxurl,
				data: {
					'action': 'aov_ajax_register',
					'reg_email'     : reg_email,
					//'reg_username'  : reg_username,
					//'reg_password'  : reg_password,
					'security'      : AOV.ajax_nonce
				},
				success: function(data){
					$('#regStatus').text(data.message);
					button.html(buttonHtml);
					button.removeAttr('disabled');
				}
			});
			e.preventDefault();
		});

		 */

		//Disable submit on enter
		$('form#formid').on('keyup keypress', function(e) {
			var keyCode = e.keyCode || e.which;
			if (keyCode === 13) {
				e.preventDefault();
				return false;
			}
		});


		// Home page slider
		$('.front-slider-wrap').bxSlider({
			auto: true,
			pause: 5000,
			mode: 'fade',
			autoHover: true,
			onSliderLoad: function() {
				var slideindex = 1;
				$('section.front-page-slider .slide').each(function () {
					slideindex++;
					var slidewrapper = $(this);
					var count = $('section.front-page-slider .slide').length;
					var nextslidebutton = $(this).find('.next-slide');

					if (slideindex > count) {slideindex = 1;}

					var nextslide = $('section.front-page-slider .slide:nth-of-type('+slideindex+')').data('slide');
					nextslidebutton.css('background-image','url('+nextslide+')');
				});
			}
		});


		/* Related articles slideshow */

		$('.front-cats-wrap').each(function () {

			var catslider = $('.front-cats-wrap').bxSlider();

			$(window).resize( function () {

				if ( $(window).outerWidth() < 375 ) {
					catslider.reloadSlider({
						touchEnabled: false,
						auto: false,
						pause: 5000,
						slideWidth: 280,
						minSlides: 1,
						maxSlides: 1,
						slideMargin: 0
					});
				}
				if ( $(window).outerWidth() >= 375 && $(window).outerWidth() < 667 ) {
					catslider.reloadSlider({
						touchEnabled: false,
						auto: false,
						pause: 5000,
						slideWidth: 330,
						minSlides: 1,
						maxSlides: 1,
						slideMargin: 0
					});
				}

				if ( $(window).outerWidth() >= 667 && $(window).outerWidth() < 767 ) {
					catslider.reloadSlider({
						touchEnabled: false,
						auto: false,
						pause: 5000,
						slideWidth: 330,
						minSlides: 2,
						maxSlides: 2,
						slideMargin: 0
					});
				}

				if ( $(window).outerWidth() >= 768 && $(window).outerWidth() < 1200 ) {
					catslider.reloadSlider({
						touchEnabled: false,
						auto: false,
						pause: 5000,
						slideWidth: 380,
						minSlides: 2,
						maxSlides: 2,
						slideMargin: 0
					});
				}

				if ( $(window).outerWidth() >= 1200 && $(window).outerWidth() < 1440 ) {
					catslider.reloadSlider({
						touchEnabled: false,
						auto: false,
						pause: 5000,
						slideWidth: 380,
						minSlides: 3,
						maxSlides: 3,
						slideMargin: 0
					});
				}

				if ( $(window).outerWidth() >= 1440 ) {
					catslider.reloadSlider({
						touchEnabled: false,
						auto: false,
						pause: 5000,
						slideWidth: 470,
						minSlides: 3,
						maxSlides: 3,
						slideMargin: 0
					});
				}

			}).resize();

		});


		/* AJAX Search */

		$('form#searchform').on( 'submit', function (e) {
			e.preventDefault();
			var searchString = $('input#s').val();

			// проверим, если в поле ввода более 2 символов, запускаем ajax
			if(searchString.length > 2){
				$('#header_search_result').html('<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>');
				$.ajax({
					url: AOV.ajaxurl,
					type: 'POST',
					data:{
						'action':'aov_ajax_search',
						'security': AOV.ajax_nonce,
						'term'  :searchString
					},
					success:function(result){
						$('#header_search_result').html(result);
					}
				});
			}

			//alert($('input#s').val());
		});


		/* Get product by id with AJAX */

		$('#header_search_result').on( 'click', 'a', function (e) {
			e.preventDefault();

			$('#header_search_result a').removeClass( 'active' );

			$(this).addClass( 'active' );

			var productid = $(this).data( 'productid' );

			// проверим, если в поле ввода более 2 символов, запускаем ajax
			if(productid){
				$( '#header_search_item' ).html( '<i class="fa fa-circle-o-notch fa-spin fa-fw"></i>' );//result);
				$.ajax({
					url: AOV.ajaxurl,
					type: 'POST',
					data:{
						'action':'aov_ajax_get_product',
						'security': AOV.ajax_nonce,
						'term'  :productid
					},
					success:function(result){
						$('#header_search_item').fadeIn().html(result);
					}
				});
			}

		});


		/* Show / hide product filter */

		$('#filterWrap').each( function () {
			$('body').addClass( 'filterOpen' );
		});

		$('body.woocommerce').on('DOMSubtreeModified', ".woof_products_top_panel", function() {
			var filtersCount = $('#main .woof_products_top_panel ul li').length;
			if ( filtersCount > 0 ) {
				$( '#filterCounter span' ).html( filtersCount ).show();
			} else {
				$( '#filterCounter span' ).html( '' ).hide();
			}
		});
		
		$('#filterCounter').on( 'click', function () {
			$('#filterWrap').toggle();
			$('body').toggleClass('filterOpen');
		});

		$('.product-slideshow-wrap').bxSlider({
			controls: false,
			auto: true,
			touchEnabled: false
		});


	}(jQuery);