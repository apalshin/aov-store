<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<figure class="news-archive">

				<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<h2 class="page-title"><?php _e('Новости'); ?></h2>
				</header><!-- .page-header -->

				<div class="row">
					<?php
					// Start the Loop.
					while ( have_posts() ) :
						the_post();

						?>

						<div class="col-12 col-md-6 pl-0 pr-0">

							<a href="<?php echo get_permalink(); ?>">

								<figure class="front-news-item hoverbox">

									<div class="hover-box-inner news-item-image" style="background-image: url('<?php echo get_the_post_thumbnail_url();?>')"></div>

									<div class="top-20">

										<div class="news-item-name">
											<div class="news-date"><?php the_time('d.m.Y'); ?></div>
											<h4 class="top-10"><?php the_title(); ?></h4>
											<div class="top-30 read-more hover-fadein"><span>Читать новость</span></div>
										</div>

									</div>

								</figure>

							</a>

						</div>

					<?php
						// End the loop.
					endwhile;

					// Previous/next page navigation.
					the_posts_pagination(
						array(
							'prev_text'          => __( 'Previous page', 'twentysixteen' ),
							'next_text'          => __( 'Next page', 'twentysixteen' ),
							'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
						)
					);

					// If no content, include the "No posts found" template.
					else :
						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>
				</div>

			</figure>
		</main><!-- .site-main -->
	</div><!-- .content-area -->
</div>


<?php get_footer(); ?>
